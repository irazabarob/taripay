import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Tienda } from 'src/app/shared/shared/model/tienda';

@Component({
  selector: 'app-tienda',
  templateUrl: './tienda.component.html',
  styleUrls: ['./tienda.component.scss']
})
export class TiendaComponent implements OnInit {

  productDialog: boolean = false;

  selectedProducts: any =[];
  tienda?: Tienda ;

  tiendaForm!:FormGroup;

  latitud: number = -12.060419;
  longitud: number = -77.040341;


  submitted: boolean = false;

  constructor(private messageService: MessageService,
              private fb : FormBuilder) { }

  ngOnInit(): void {
    this.inicializar();
  }

  inicializar(){
    this.tiendaForm = this.fb.group({
      codigo : ['', Validators.required],
      descripcion : ['', Validators.required],
      direccion : ['', Validators.required],
      coordenada : ['', Validators.required],
      rubro : ['', Validators.required],
    })
  }

  openNew() {
    // this.product = {};
    this.submitted = false;
    this.productDialog = true;
}

deleteSelectedProducts() {
    // this.confirmationService.confirm({
    //     message: 'Are you sure you want to delete the selected products?',
    //     header: 'Confirm',
    //     icon: 'pi pi-exclamation-triangle',
    //     accept: () => {
    //         this.products = this.products.filter(val => !this.selectedProducts.includes(val));
    //         this.selectedProducts = null;
    //         this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
    //     }
    // });
}

editProduct(product: any ) {
    // this.product = {...product};
    // this.productDialog = true;
}

deleteProduct(product: any) {
    // this.confirmationService.confirm({
    //     message: 'Are you sure you want to delete ' + product.name + '?',
    //     header: 'Confirm',
    //     icon: 'pi pi-exclamation-triangle',
    //     accept: () => {
    //         this.products = this.products.filter(val => val.id !== product.id);
    //         this.product = {};
    //         this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Deleted', life: 3000});
    //     }
    // });
}

hideDialog() {
    // this.productDialog = false;
    // this.submitted = false;
}

saveProduct() {
    // this.submitted = true;

    // if (this.product.name.trim()) {
    //     if (this.product.id) {
    //         this.products[this.findIndexById(this.product.id)] = this.product;                
    //         this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Updated', life: 3000});
    //     }
    //     else {
    //         this.product.id = this.createId();
    //         this.product.image = 'product-placeholder.svg';
    //         this.products.push(this.product);
    //         this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Created', life: 3000});
    //     }

    //     this.products = [...this.products];
    //     this.productDialog = false;
    //     this.product = {};
    // }
}

findIndexById(id: string): number {
    let index = -1;
    // for (let i = 0; i < this.products.length; i++) {
    //     if (this.products[i].id === id) {
    //         index = i;
    //         break;
    //     }
    // }

    return index;
}

createId(): string {
  let id = '';
    // var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    // for ( var i = 0; i < 5; i++ ) {
    //     id += chars.charAt(Math.floor(Math.random() * chars.length));
    // }
  return id;
}

obgenerCoordenada(event: any): void{
console.log(event)
  this.latitud = event.latLng.lat();;
  this.longitud = event.latLng.lng() ;
}

}
