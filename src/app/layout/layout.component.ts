import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  body: any;
  sidebar:any;
  toggle: any;
  searchBox: any;
  modeSwitch: any;
  modeText:any
  mode: string = "Dark Mode";

  constructor() { }

  ngOnInit(): void {
    
  }

  switchMode(e: Event){
    let body = document.querySelector(".contenedor-sidebar");
    let sidebar = body?.querySelector(".sidebar");
    let toggle = body?.querySelector(".toggle");
    let searchBtn = body?.querySelector(".search-box");
    let modeSwitch = body?.querySelector(".toggle-switch");
    let modeText = body?.querySelector(".mode-text");

    body?.classList.toggle("dark");
  
    if(this.mode === "Dark Mode"){
      this.mode = "Light Mode"
    } else {
      this.mode = "Dark Mode";
    } 
  }

  toggleEvent(){
    let body = document.querySelector(".contenedor-sidebar");
    let sidebar = body?.querySelector(".sidebar");
    let toggle = body?.querySelector(".toggle");
    let modeText = body?.querySelector(".mode-text");
    sidebar?.classList.toggle("close");   
  
  }

}
