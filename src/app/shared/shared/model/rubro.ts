export interface Rubro {
    rubroId: number;
    codigo: string;
    descripcion: string;
}