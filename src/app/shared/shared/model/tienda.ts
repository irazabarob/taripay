import { Rubro } from "./rubro";
import { Usuario } from "./usuario";

export interface Tienda {
    tiendaId: number;    
    codigo : string;    
    descripcion: string;
    coordenada : string;    
    direccion : string;
    usuario : Usuario;
    rubro : Rubro;
}